﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationStudenciMVC.Models;

namespace WebApplicationStudenciMVC.Controllers
{
    public class PrzedmiotiesController : Controller
    {
        private StudenciWebAppEntities db = new StudenciWebAppEntities();

        // GET: Przedmioties
        public ActionResult Index()
        {
            return View(db.Przedmioty.ToList());
        }

        // GET: Przedmioties/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Przedmioty przedmioty = db.Przedmioty.Find(id);
            if (przedmioty == null)
            {
                return HttpNotFound();
            }
            return View(przedmioty);
        }

        // GET: Przedmioties/Create
        public ActionResult Create()
        {
            PopulateProwadzacyDropDownList();
            PopulateSpecjalizacjaDropDownList();
            return View();
        }

        // POST: Przedmioties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_przedmiotu,ID_prowadzacego,ID_Specjalizacji,Nazwa_przedmiotu,ECTS")] Przedmioty przedmioty)
        {
            if (ModelState.IsValid)
            {
                db.Przedmioty.Add(przedmioty);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateProwadzacyDropDownList(przedmioty.ID_prowadzacego);
            PopulateSpecjalizacjaDropDownList(przedmioty.ID_Specjalizacji);
            return View(przedmioty);
        }

        // GET: Przedmioties/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Przedmioty przedmioty = db.Przedmioty.Find(id);
            if (przedmioty == null)
            {
                return HttpNotFound();
            }
            return View(przedmioty);
        }

        // POST: Przedmioties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_przedmiotu,ID_prowadzacego,ID_Specjalizacji,Nazwa_przedmiotu,ECTS")] Przedmioty przedmioty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(przedmioty).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(przedmioty);
        }

        // GET: Przedmioties/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Przedmioty przedmioty = db.Przedmioty.Find(id);
            if (przedmioty == null)
            {
                return HttpNotFound();
            }
            return View(przedmioty);
        }

        // POST: Przedmioties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Przedmioty przedmioty = db.Przedmioty.Find(id);
            db.Przedmioty.Remove(przedmioty);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void PopulateProwadzacyDropDownList(object selectedProwadzacy = null)
        {
            var prowadzacyQuery = from d in db.Prowadzacy
                                     orderby d.ID_prowadzacego
                                     select d;
            ViewBag.ID_prowadzacego = new SelectList(prowadzacyQuery, "ID_prowadzacego", "P_Nazwisko", selectedProwadzacy);
        }
        private void PopulateSpecjalizacjaDropDownList(object selectedSpecjalizacja = null)
        {
            var specjalizacjaQuery = from d in db.Specjalizacja
                                  orderby d.ID_Specjalizacji
                                  select d;
            ViewBag.ID_Specjalizacji = new SelectList(specjalizacjaQuery, "ID_Specjalizacji", "Nazwa_specjalizacji", selectedSpecjalizacja);
        }
    }
}
