﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationStudenciMVC.Models;

namespace WebApplicationStudenciMVC.Controllers
{
    public class ProjektiesController : Controller
    {
        private StudenciWebAppEntities db = new StudenciWebAppEntities();

        // GET: Projekties
        public ActionResult Index()
        {
            return View(db.Projekty.ToList());
        }

        // GET: Projekties/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projekty projekty = db.Projekty.Find(id);
            if (projekty == null)
            {
                return HttpNotFound();
            }
            return View(projekty);
        }

        // GET: Projekties/Create
        public ActionResult Create()
        {
            PopulateProjektyDropDownList();
            return View();
        }

        // POST: Projekties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Projektu,ID_przedmiotu,Nazwa_projektu,Data_projektu")] Projekty projekty)
        {
            if (ModelState.IsValid)
            {
                db.Projekty.Add(projekty);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateProjektyDropDownList(projekty.ID_przedmiotu);
            return View(projekty);
        }

        // GET: Projekties/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projekty projekty = db.Projekty.Find(id);
            if (projekty == null)
            {
                return HttpNotFound();
            }
            return View(projekty);
        }

        // POST: Projekties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Projektu,ID_przedmiotu,Nazwa_projektu,Data_projektu")] Projekty projekty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(projekty).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(projekty);
        }

        // GET: Projekties/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projekty projekty = db.Projekty.Find(id);
            if (projekty == null)
            {
                return HttpNotFound();
            }
            return View(projekty);
        }

        // POST: Projekties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Projekty projekty = db.Projekty.Find(id);
            db.Projekty.Remove(projekty);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void PopulateProjektyDropDownList(object selectedPrzedmiot = null)
        {
            var projektyQuery = from d in db.Przedmioty
                                     orderby d.ID_przedmiotu
                                     select d;
            ViewBag.ID_przedmiotu = new SelectList(projektyQuery, "ID_przedmiotu", "Nazwa_przedmiotu", selectedPrzedmiot);
        }
    }
}
