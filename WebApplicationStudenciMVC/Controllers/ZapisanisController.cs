﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationStudenciMVC.Models;

namespace WebApplicationStudenciMVC.Controllers
{
    public class ZapisanisController : Controller
    {
        private StudenciWebAppEntities db = new StudenciWebAppEntities();

        // GET: Zapisanis
        public ActionResult Index()
        {
            return View(db.Zapisani.ToList());
        }

        // GET: Zapisanis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zapisani zapisani = db.Zapisani.Find(id);
            if (zapisani == null)
            {
                return HttpNotFound();
            }
            return View(zapisani);
        }

        // GET: Zapisanis/Create
        public ActionResult Create()
        {
            PopulateZapisStudentProjDropDownList();
            PopulateZapisProjektProjDropDownList();
            return View();
        }

        // POST: Zapisanis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Zapisani,Z_Student,Z_Projekt,Z_Termin")] Zapisani zapisani)
        {
            if (ModelState.IsValid)
            {
                db.Zapisani.Add(zapisani);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateZapisStudentProjDropDownList(zapisani.Z_Student);
            PopulateZapisProjektProjDropDownList(zapisani.Z_Projekt);
            return View(zapisani);
        }

        // GET: Zapisanis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zapisani zapisani = db.Zapisani.Find(id);
            if (zapisani == null)
            {
                return HttpNotFound();
            }
            return View(zapisani);
        }

        // POST: Zapisanis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Zapisani,Z_Student,Z_Projekt,Z_Termin")] Zapisani zapisani)
        {
            if (ModelState.IsValid)
            {
                db.Entry(zapisani).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(zapisani);
        }

        // GET: Zapisanis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zapisani zapisani = db.Zapisani.Find(id);
            if (zapisani == null)
            {
                return HttpNotFound();
            }
            return View(zapisani);
        }

        // POST: Zapisanis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Zapisani zapisani = db.Zapisani.Find(id);
            db.Zapisani.Remove(zapisani);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void PopulateZapisStudentProjDropDownList(object selectedOczekujacy = null)
        {
            var oczekujacyDoZapisuStudentQuery = from d in db.Oczekujacy
                                     orderby d.O_indeks
                                     select d;
            ViewBag.Z_Student = new SelectList(oczekujacyDoZapisuStudentQuery, "O_indeks", "O_indeks", selectedOczekujacy);
        }

        private void PopulateZapisProjektProjDropDownList(object selectedOczekujacy = null)
        {
            var oczekujacyDoZapisuProjektQuery = from d in db.Oczekujacy
                                                 orderby d.O_projekt
                                                 select d;
            ViewBag.Z_Projekt = new SelectList(oczekujacyDoZapisuProjektQuery, "O_projekt", "O_projekt", selectedOczekujacy);
        }
    }
}
