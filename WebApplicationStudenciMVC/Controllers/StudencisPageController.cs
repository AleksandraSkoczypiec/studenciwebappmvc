﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationStudenciMVC.Models;

namespace WebApplicationStudenciMVC.Controllers
{
    public class StudencisPageController : Controller
    {
        private StudenciWebAppEntities db = new StudenciWebAppEntities();

        // GET: StudencisPage
        public ActionResult Index()
        {
            return View(db.Studenci.ToList());
        }

        // GET: StudencisPage/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Studenci studenci = db.Studenci.Find(id);
            if (studenci == null)
            {
                return HttpNotFound();
            }
            return View(studenci);
        }

        // GET: StudencisPage/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StudencisPage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Studenta,ID_Specjalizacji,ST_Imie,ST_Nazwisko,ST_Adres,ST_Nr_indeksu,ST_Data_urodzenia")] Studenci studenci)
        {
            if (ModelState.IsValid)
            {
                db.Studenci.Add(studenci);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(studenci);
        }

        // GET: StudencisPage/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Studenci studenci = db.Studenci.Find(id);
            if (studenci == null)
            {
                return HttpNotFound();
            }
            return View(studenci);
        }

        // POST: StudencisPage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Studenta,ID_Specjalizacji,ST_Imie,ST_Nazwisko,ST_Adres,ST_Nr_indeksu,ST_Data_urodzenia")] Studenci studenci)
        {
            if (ModelState.IsValid)
            {
                db.Entry(studenci).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(studenci);
        }

        // GET: StudencisPage/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Studenci studenci = db.Studenci.Find(id);
            if (studenci == null)
            {
                return HttpNotFound();
            }
            return View(studenci);
        }

        // POST: StudencisPage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Studenci studenci = db.Studenci.Find(id);
            db.Studenci.Remove(studenci);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
