﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationStudenciMVC.Models;

namespace WebApplicationStudenciMVC.Controllers
{
    public class Oceny_projektyController : Controller
    {
        private StudenciWebAppEntities db = new StudenciWebAppEntities();

        // GET: Oceny_projekty
        public ActionResult Index()
        {
            return View(db.Oceny_projekty);
        }

        // GET: Oceny_projekty/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny_projekty oceny_projekty = db.Oceny_projekty.Find(id);
            if (oceny_projekty == null)
            {
                return HttpNotFound();
            }
            return View(oceny_projekty);
        }

        // GET: Oceny_projekty/Create
        public ActionResult Create()
        {
            PopulateProjektyDropDownList();
            PopulateStudentDropDownList();
            return View();
        }

        // POST: Oceny_projekty/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OProj_Id,ID_Projektu,ID_Studenta,OProj_Ocena,OProj_Data")] Oceny_projekty oceny_projekty)
        {
            if (ModelState.IsValid)
            {
                db.Oceny_projekty.Add(oceny_projekty);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateProjektyDropDownList(oceny_projekty.ID_Projektu);
            PopulateStudentDropDownList(oceny_projekty.ID_Studenta);
            return View(oceny_projekty);
        }

        // GET: Oceny_projekty/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny_projekty oceny_projekty = db.Oceny_projekty.Find(id);
            if (oceny_projekty == null)
            {
                return HttpNotFound();
            }
            return View(oceny_projekty);
        }

        // POST: Oceny_projekty/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OProj_Id,ID_Projektu,ID_Studenta,OProj_Ocena,OProj_Data")] Oceny_projekty oceny_projekty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oceny_projekty).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oceny_projekty);
        }

        // GET: Oceny_projekty/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny_projekty oceny_projekty = db.Oceny_projekty.Find(id);
            if (oceny_projekty == null)
            {
                return HttpNotFound();
            }
            return View(oceny_projekty);
        }

        // POST: Oceny_projekty/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oceny_projekty oceny_projekty = db.Oceny_projekty.Find(id);
            db.Oceny_projekty.Remove(oceny_projekty);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void PopulateProjektyDropDownList(object selectedProjekt = null)
        {
            var projektyQuery = from d in db.Projekty
                                orderby d.ID_Projektu
                                select d;
            ViewBag.ID_Projektu = new SelectList(projektyQuery, "ID_Projektu", "Nazwa_projektu", selectedProjekt);
        }
        private void PopulateStudentDropDownList(object selectedStudent = null)
        {
            var studenciQuery = from d in db.Studenci
                                orderby d.ID_Studenta
                                select d;
            ViewBag.ID_Studenta = new SelectList(studenciQuery, "ID_Studenta", "ST_Nr_indeksu", selectedStudent);
        }
    }
}
