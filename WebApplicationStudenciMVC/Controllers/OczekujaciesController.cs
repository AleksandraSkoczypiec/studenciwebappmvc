﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationStudenciMVC.Models;

namespace WebApplicationStudenciMVC.Controllers
{
    public class OczekujaciesController : Controller
    {
        private StudenciWebAppEntities db = new StudenciWebAppEntities();

        // GET: Oczekujacies
        public ActionResult Index()
        {
            return View(db.Oczekujacy.ToList());
            //return RedirectToAction("Index","StudencisPage");
        }

        // GET: Oczekujacies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oczekujacy oczekujacy = db.Oczekujacy.Find(id);
            if (oczekujacy == null)
            {
                return HttpNotFound();
            }
            return View(oczekujacy);
        }

        // GET: Oczekujacies/Create
        public ActionResult Create()
        {
            PopulateStudentDropDownList();
            PopulateProjektyDropDownList();
            return View();
        }

        // POST: Oczekujacies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "O_ID,O_indeks,O_projekt")] Oczekujacy oczekujacy)
        {
            if (ModelState.IsValid)
            {
                db.Oczekujacy.Add(oczekujacy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateStudentDropDownList(oczekujacy.O_indeks);
            PopulateProjektyDropDownList(oczekujacy.O_projekt);
            return View(oczekujacy);
        }

        // GET: Oczekujacies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oczekujacy oczekujacy = db.Oczekujacy.Find(id);
            if (oczekujacy == null)
            {
                return HttpNotFound();
            }
            return View(oczekujacy);
        }

        // POST: Oczekujacies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "O_ID,O_indeks,O_projekt")] Oczekujacy oczekujacy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oczekujacy).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oczekujacy);
        }

        // GET: Oczekujacies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oczekujacy oczekujacy = db.Oczekujacy.Find(id);
            if (oczekujacy == null)
            {
                return HttpNotFound();
            }
            return View(oczekujacy);
        }

        // POST: Oczekujacies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oczekujacy oczekujacy = db.Oczekujacy.Find(id);
            db.Oczekujacy.Remove(oczekujacy);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void PopulateStudentDropDownList(object selectedStudent = null)
        {
            var studenciQuery = from d in db.Studenci
                                orderby d.ST_Nr_indeksu
                                select d;
            ViewBag.O_indeks = new SelectList(studenciQuery, "ST_Nr_indeksu", "ST_Nr_indeksu", selectedStudent);
        }
        private void PopulateProjektyDropDownList(object selectedPrzedmiot = null)
        {
            var projektyQuery = from d in db.Projekty
                                orderby d.Nazwa_projektu
                                select d;
            ViewBag.O_projekt = new SelectList(projektyQuery, "Nazwa_projektu", "Nazwa_projektu", selectedPrzedmiot);
        }
    }
}
