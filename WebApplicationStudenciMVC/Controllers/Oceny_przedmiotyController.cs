﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationStudenciMVC.Models;

namespace WebApplicationStudenciMVC.Controllers
{
    public class Oceny_przedmiotyController : Controller
    {
        private StudenciWebAppEntities db = new StudenciWebAppEntities();

        // GET: Oceny_przedmioty
        public ActionResult Index()
        {
            return View(db.Oceny_przedmioty.ToList());
        }

        // GET: Oceny_przedmioty/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny_przedmioty oceny_przedmioty = db.Oceny_przedmioty.Find(id);
            if (oceny_przedmioty == null)
            {
                return HttpNotFound();
            }
            return View(oceny_przedmioty);
        }

        // GET: Oceny_przedmioty/Create
        public ActionResult Create()
        {
            PopulateStudentDropDownList();
            PopulatePrzedmiotyDropDownList();
            return View();
        }

        // POST: Oceny_przedmioty/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OPrz_Id,ID_Studenta,ID_przedmiotu,OPrz_Ocena,Oprz_Data")] Oceny_przedmioty oceny_przedmioty)
        {
            if (ModelState.IsValid)
            {
                db.Oceny_przedmioty.Add(oceny_przedmioty);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateStudentDropDownList(oceny_przedmioty.ID_Studenta);
            PopulatePrzedmiotyDropDownList(oceny_przedmioty.ID_przedmiotu);
            return View(oceny_przedmioty);
        }

        // GET: Oceny_przedmioty/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny_przedmioty oceny_przedmioty = db.Oceny_przedmioty.Find(id);
            if (oceny_przedmioty == null)
            {
                return HttpNotFound();
            }
            return View(oceny_przedmioty);
        }

        // POST: Oceny_przedmioty/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OPrz_Id,ID_Studenta,ID_przedmiotu,OPrz_Ocena,Oprz_Data")] Oceny_przedmioty oceny_przedmioty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oceny_przedmioty).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oceny_przedmioty);
        }

        // GET: Oceny_przedmioty/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny_przedmioty oceny_przedmioty = db.Oceny_przedmioty.Find(id);
            if (oceny_przedmioty == null)
            {
                return HttpNotFound();
            }
            return View(oceny_przedmioty);
        }

        // POST: Oceny_przedmioty/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oceny_przedmioty oceny_przedmioty = db.Oceny_przedmioty.Find(id);
            db.Oceny_przedmioty.Remove(oceny_przedmioty);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private void PopulateStudentDropDownList(object selectedStudent = null)
        {
            var studenciQuery = from d in db.Studenci
                                orderby d.ID_Studenta
                                select d;
            ViewBag.ID_Studenta = new SelectList(studenciQuery, "ID_Studenta", "ST_Nr_indeksu", selectedStudent);
        }
        private void PopulatePrzedmiotyDropDownList(object selectedPrzedmiot = null)
        {
            var przedmiotQuery = from d in db.Przedmioty
                                orderby d.ID_przedmiotu
                                select d;
            ViewBag.ID_przedmiotu = new SelectList(przedmiotQuery, "ID_przedmiotu", "Nazwa_przedmiotu", selectedPrzedmiot);
        }

    }
}
