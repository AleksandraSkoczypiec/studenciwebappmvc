﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationStudenciMVC.Models;

namespace WebApplicationStudenciMVC.Controllers
{
    public class SpecjalizacjasController : Controller
    {
        private StudenciWebAppEntities db = new StudenciWebAppEntities();

        // GET: Specjalizacjas
        public ActionResult Index()
        {
            return View(db.Specjalizacja.ToList());
        }

        // GET: Specjalizacjas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Specjalizacja specjalizacja = db.Specjalizacja.Find(id);
            if (specjalizacja == null)
            {
                return HttpNotFound();
            }
            return View(specjalizacja);
        }

        // GET: Specjalizacjas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Specjalizacjas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_Specjalizacji,Nazwa_specjalizacji")] Specjalizacja specjalizacja)
        {
            if (ModelState.IsValid)
            {
                db.Specjalizacja.Add(specjalizacja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(specjalizacja);
        }

        // GET: Specjalizacjas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Specjalizacja specjalizacja = db.Specjalizacja.Find(id);
            if (specjalizacja == null)
            {
                return HttpNotFound();
            }
            return View(specjalizacja);
        }

        // POST: Specjalizacjas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_Specjalizacji,Nazwa_specjalizacji")] Specjalizacja specjalizacja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specjalizacja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(specjalizacja);
        }

        // GET: Specjalizacjas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Specjalizacja specjalizacja = db.Specjalizacja.Find(id);
            if (specjalizacja == null)
            {
                return HttpNotFound();
            }
            return View(specjalizacja);
        }

        // POST: Specjalizacjas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Specjalizacja specjalizacja = db.Specjalizacja.Find(id);
            db.Specjalizacja.Remove(specjalizacja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
